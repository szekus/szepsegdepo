<?php

echo '<pre>';


require_once('loader.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');
require_once('settings.php');

$db = \db\Database::instance();

$query = "SELECT * FROM groups_items_lang WHERE lang = 'hu' AND id_item IN (SELECT id_item FROM groups_items WHERE id_group = 7)";
$manufacturers = $db->query($query);

$manufacturer = \resources\Manufacturer::create();
$manufacturer->deleteAll();

$request = resources\Request::create();
foreach ($manufacturers as $value) {


    $manufacturer = \resources\Manufacturer::create();
    $manufacturerId = $value["id_item"];
    $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $manufacturerId);
    $manufacturer->id = $manufacturerId;
    $manufacturer->innerId = $manufacturerId;
    $manufacturer->name = $value["title"];
    $manufacturer->sortOrder = 0;
    $manufacturer->picture = "";


    $request->addBatch($manufacturer->createBatchArray());
//    $manufacturer->insert(true);

    $manufacturerDescription = resources\ManufacturerDescription::create();
    $manufacturerDescription->language = array("id" => LANG_HU);
    $manufacturerDescription->manufacturer = array("id" => $manufacturerSRId);
    $manufacturerDescription->metaDescription = "";
    $manufacturerDescription->metaKeywords = "";
    $manufacturerDescription->description = "";

//    $manufacturerDescription->insert(true);
    $request->addBatch($manufacturerDescription->createBatchArray());
}

$request->run();

