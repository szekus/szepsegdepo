<?php
namespace classes;

class CSVCleaner
{
    private $rows;

    public function __construct(array $rows)
    {
        $this->rows = $rows;
    }
    public function removeEmptyLines()
    {
        $newRows = [];
        foreach($this->rows as $row) {
          if (str_replace(',','',implode(',', $row))!='') {
              $newRows[] = $row;
          }
        }
        $this->rows = $newRows;
    }
    public function dropRowIfValueIn($args) {
        $newRows = [];
        $columnNumber = $args['columnNumber'];
        $values = $args['values'];

        foreach($this->rows as $row) {
        //  echo ':~'.$row[$columnNumber].':'.in_array($row[$columnNumber], $values);

          if (isset($row[$columnNumber]) && !in_array($row[$columnNumber], $values))   {
              $newRows[] = $row;
          //    echo 'benen';
          }
          // echo '<br>';
        }
        $this->rows = $newRows;
    }
    public function dropRowIfValueEmpty($args) {
        $newRows = [];
        $columnNumber = $args['columnNumber'];

        foreach($this->rows as $row) {
        //  echo ':~'.$row[$columnNumber].':'.in_array($row[$columnNumber], $values);

          if (isset($row[$columnNumber]) && $row[$columnNumber]!='')   {
              $newRows[] = $row;
          //    echo 'benen';
          }
          // echo '<br>';
        }
        $this->rows = $newRows;
    }
    public function getCleaned()
    {
        return $this->rows;
    }
}
