<?php

echo '<pre>';

require_once('loader.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');
require_once('settings.php');


$db = \db\Database::instance();

$query = "SELECT * FROM categ AS c " .
        "INNER JOIN categ_lang AS cl on c.id_categ = cl.id_categ AND cl.lang = 'hu' " .
        "INNER JOIN seo_url AS su ON su.mod = 'C' AND su.item_id = c.id_categ AND su.lang = 'hu'";

$categories = $db->query($query);
$categoryUrls = array();
for ($i = 0; $i < count($categories); $i++) {
    $category = $categories[$i];

    $url = $category["url"];


    $oldUrl = str_replace(SHOP_URL, "", $url);
    $newUrl = str_replace(".html", "", $oldUrl);
    $arr = explode("/", $url);

    $newUrl = $arr[count($arr) - 1];
    $newUrl = str_replace(".html", "", $newUrl);
    $categoryUrls[] = array(
        "oldUrl" => $oldUrl,
        "newUrl" => $newUrl,
        "order" => 0
    );
}



$fp = fopen('data/szepsegdepoCategoryUrl.csv', 'w');

$headers = ['old_url', 'new_url', 'order'];
fputcsv($fp, $headers, ";");
foreach ($categoryUrls as $fields) {
    fputcsv($fp, array_values($fields), ";");
}
fclose($fp);

$oldURLlink = "http://szepsegdepo.hu/";
$newURLlink = "https://szepsegdepo.shoprenter.hu/";

foreach ($categoryUrls as $value) {
    echo "<a href='" . $oldURLlink . $value["oldUrl"] . "' target='_blank'>" . $oldURLlink . $value["oldUrl"] . "</a> || "
    . "<a href='" . $newURLlink . $value["newUrl"] . "' target='_blank'>" . $newURLlink . $value["newUrl"] . "</a> || ";
    echo '<br>';
}