<?php

echo '<pre>';

require_once('loader.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');
require_once('settings.php');


$db = \db\Database::instance();

$query = "SELECT * FROM categ AS c " .
        "INNER JOIN categ_lang AS cl on c.id_categ = cl.id_categ AND cl.lang = 'hu' " .
        "INNER JOIN seo_url AS su ON su.mod = 'C' AND su.item_id = c.id_categ AND su.lang = 'hu'";

$categories = $db->query($query);

$categoryResource = resources\Category::create();
$categoryResource->deleteAll();

$urlAliasResource = resources\URLAlias::create();
$urlAliasResource->deleteCategoryUrl();


$request = resources\Request::create();
for ($i = 0; $i < count($categories); $i++) {
    $category = $categories[$i];
    $categoryResource = resources\Category::create();
    $categoryId = $category["id_categ"];
    $categorySRId = base64_encode("category-category_id=" . $categoryId);
    $categoryResource->id = $categoryId;
    $categoryResource->innerId = $categoryId;
    $status = 1;
    if ($category["status"] == "N") {
        $status = 0;
    }

    $categoryResource->status = $status;
    $categoryResource->sortOrder = $category["ordered"];

    $categoryResource->picture = "categ/" . $category["image"];
//    sout($categoryId);


    $request->addBatch($categoryResource->createBatchArray());
//    $categoryResource->insert(true);



    $categoryDescriptionResource = \resources\CategoryDescription::create();
    $categoryDescriptionResource->name = $category["title"];
    $categoryDescriptionResource->category = array("id" => $categorySRId);
    $categoryDescriptionResource->language = array("id" => LANG_HU);
    $categoryDescriptionResource->metaKeywords = $category["meta_desc"];
    $categoryDescriptionResource->metaDescription = $category["meta_keyw"];
    $categoryDescriptionResource->description = "";

//    $categoryDescriptionResource->insert(true);
    $request->addBatch($categoryDescriptionResource->createBatchArray());

    $urlAliasResource = \resources\URLAlias::create();
    $urlAliasResource->type = "CATEGORY";
    $urlAliasResource->urlAliasEntity = array("id" => $categorySRId);

    $url = str_replace("https://szepsegdepo.hu/", "", $category["url"]);
    $url = str_replace(".html", "", $url);
    $arr = explode("/", $url);

    $lastPart = $arr[count($arr) - 1];
    $urlAliasResource->urlAlias = $lastPart;

    $request->addBatch($urlAliasResource->createBatchArray());
//    $urlAliasResource->insert(true);
}



$request->run();

for ($i = 0; $i < count($categories); $i++) {
    $category = $categories[$i];
    $categoryResource = resources\Category::create();

    $childId = base64_encode("category-category_id=" . $category["id_categ"]);
    $parentId = base64_encode("category-category_id=" . $category["parent"]);
    $categoryResource->id = $childId;
    $categoryResource->parentCategory = array("id" => $parentId);

    if ($parentId != "0") {
        $categoryResource->update(true);
    }
}