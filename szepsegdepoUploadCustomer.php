<?php

echo '<pre>';
define("LIMIT", -1);
define('SR_DEFAULT_COUNTRY', 'Y291bnRyeS1jb3VudHJ5X2lkPTk3');   // Magyarország
//use resources\Manufacturer;


require_once('loader.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');
require_once('settings.php');

$db = \db\Database::instance();
$countryResource = \resources\Country::create();

$customerResource = \resources\Customer::create();
$customerResource->deleteAll();
//
//$countryResource->createDBTable();
//$array = $countryResource->getAll();
//$countryResource->insertToDB($array);
//die();

$query = "SELECT * FROM user";

$users = $db->query($query);

$request = resources\Request::create();
foreach ($users as $user) {


    $query = "SELECT * FROM user AS u " .
            "INNER JOIN user_det AS ud ON u.id_user = ud.id_user AND u.id_user = " . $user["id_user"];

    $userResult = $db->query($query);


    $userTmp = array();
    foreach ($userResult as $value) {
        $userTmp[] = $value;
    }


    $customer = \resources\Customer::create();
    $customerSRId = $customer->createSRId($user["id_user"]);
    $customer->id = $user["id_user"];
    $customer->innerId = $user["id_user"];
    $customer->email = $user["email"];

    $customer->password = $user["password"];
    $customer->status = 1;
    $customer->approved = 1;
    $customer->newsletter = 0;

    $customerGroup = SR_DEFAULT_CUSTOMER_GROUP;
    switch ($user["id_va"]) {
        case "1":
            $customerGroup = SR_VASARLOI_CSOPORT_1;
            break;
        case "2":
            $customerGroup = SR_VASARLOI_CSOPORT_2;
            break;
        case "3":
            $customerGroup = SR_VASARLOI_CSOPORT_3;
            break;
    }

    $customer->customerGroup = array("id" => $customerGroup);

    foreach ($userTmp as $userValue) {
        if ($userValue["fullname"] != "") {
            $fullname = split_name($userValue["fullname"]);
            $customer->firstname = $fullname["firstname"];
            $customer->lastname = $fullname["lastname"];
        }
        if ($customer->telephone != "") {
            $customer->telephone = $user["telefon"];
        }
    }



    if ($customer->firstname == "") {
        $customer->firstname = "Nincs megadva";
    }
    if ($customer->lastname == "") {
        $customer->lastname = "Nincs megadva";
    }
    if ($customer->telephone == "") {
        $customer->telephone = 1;
    }



//    $resultCustomer = $customer->insert(true);
    $request->addBatch($customer->createBatchArray());

//    sout($customer->getData());
    foreach ($userTmp as $user) {


        $addressResource = \resources\Address::create();
        if ($user["fullname"] != "") {
            $fullname = split_name($user["fullname"]);
            $addressResource->firstname = $fullname["firstname"];
            $addressResource->lastname = $fullname["lastname"];
        } else {
            $addressResource->firstname = "Nincs megadva";
            $addressResource->lastname = "Nincs megadva";
        }

        if ($user["codfisc"] != "") {
            $addressResource->taxNumber = $user["codfisc"];
        }

        if ($user["cim"] != "") {
            $addressResource->address1 = $user["cim"];
        } else {
            $addressResource->address1 = "Nincs megadva";
        }

        if ($user["postakod"] != "") {
            $addressResource->postcode = $user["postakod"];
        } else {
            $addressResource->postcode = "0";
        }

        if ($user["varos"] != "") {
            $addressResource->city = $user["varos"];
        } else {
            $addressResource->city = "Nincs megadva";
        }


        $addressResource->customer = array("id" => $customerSRId);
        $addressResource->country = array("id" => $countryResource->getSRCountryIdByIsoCode2($user["lang"]));
//        sout($addressResource->getData());
//        $addressResource->insert(true);
        $request->addBatch($addressResource->createBatchArray());
    }
}

//sout(($request->getRequest()));
$request->run();
