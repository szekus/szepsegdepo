<?php
require_once('loader.php');
require_once('helpers\shoprenter.php');
require_once('settings.php');

define('ENABLE_CUSTOMER_CLEANER', false);
define('ENABLE_CATEGORY_CLEANER', false);
define('ENABLE_URL_CLEANER', false);
define('URL_CLEANER_TYPE', 'PRODUCT'); // PRODUCT, CATEGORY, MANUFACTURER.

define('ENABLE_PRODUCT_CLEANER', false);

define('ENABLE_PRODUCT_CLASS_CLEANER', false);
define('ENABLE_PRODUCT_ATTRIBUTE_CLEANER', false);
define('ENABLE_PRODUCT_ATTRIBUTE_VALUE_CLEANER', false);
define('ENABLE_PRODUCT_ATTRIBUTE_VALUE_DESC_CLEANER', false);

// Customers Group
/*
$endpointPath = "/customerGroups";
$result = querySRApi($endpointPath, [], 'GET');
foreach($result['items'] as $item) {
  var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
}
*/

// Users / Customers
if (ENABLE_CUSTOMER_CLEANER) {
  $endpointPath = "/customers?page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          'uri' => $item['href']
      ];
      // user data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

// Termekek / Product
if (ENABLE_PRODUCT_CLEANER) {
  $endpointPath = API_ENDPOINT_PRODUCT."?page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          'uri' => $item['href']
      ];
      //  data dump -   var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
       print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

// Kategoria / Categories
if (ENABLE_CATEGORY_CLEANER) {
  echo 'start category cleaner<br>';

  $endpointPath = API_ENDPOINT_CATEGORY."?page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          'uri' => $item['href']
      ];
      //  data dump -   var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
       print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

if (ENABLE_URL_CLEANER) {
    echo 'start product url cleaner<br>';
  $endpointPath = API_ENDPOINT_URLALIASES."?type=".URL_CLEANER_TYPE."&page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          // 'method' => 'GET',
          'uri' => $item['href']
      ];
      //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

echo "Cleaning done ".date('Y-m-d H:i:s');

if (ENABLE_PRODUCT_CLASS_CLEANER) {
    echo 'start product class cleaner<br>';
  $endpointPath = API_ENDPOINT_PRODUCT_CLASS."?page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          // 'method' => 'GET',
          'uri' => $item['href']
      ];
      //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

echo "Cleaning done ".date('Y-m-d H:i:s');

if (ENABLE_PRODUCT_ATTRIBUTE_CLEANER) {
    echo 'start product class cleaner<br>';
  $endpointPath = API_ENDPOINT_LIST_ATTRIBUTE."?page=0&limit=200";

  do {
    $result = querySRApi($endpointPath, [], 'GET');
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      // print_r($item);
      $batchRequest['requests'][] = [
          'method' => 'DELETE',
          // 'method' => 'GET',
          'uri' => $item['href']
      ];
      //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
    }
    // TORLES
    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

echo "Cleaning done ".date('Y-m-d H:i:s');


//if (ENABLE_PRODUCT_ATTRIBUTE_VALUE_CLEANER) {
//    echo 'start product class cleaner<br>';
//  $endpointPath = API_ENDPOINT_LIST_ATTRIBUTE_VALUES."?page=0&limit=200";
//
//  do {
//    $result = querySRApi($endpointPath, [], 'GET');
//    $batchRequest['requests'] = [];
//    foreach($result['items'] as $item) {
//      // print_r($item);
//      $batchRequest['requests'][] = [
//          'method' => 'DELETE',
//          // 'method' => 'GET',
//          'uri' => $item['href']
//      ];
//      //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
//    }
//    // TORLES
//    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
//        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
//    }
//  } while (!is_null($result['next']));
//}
//
//echo "Cleaning done ".date('Y-m-d H:i:s');
//
//if (ENABLE_PRODUCT_ATTRIBUTE_VALUE_DESC_CLEANER) {
//    echo 'start product class cleaner<br>';
//  $endpointPath = API_ENDPOINT_LIST_ATTRIBUTE_VALUE_DESC."?page=0&limit=200";
//
//  do {
//    $result = querySRApi($endpointPath, [], 'GET');
//    $batchRequest['requests'] = [];
//    foreach($result['items'] as $item) {
//      // print_r($item);
//      $batchRequest['requests'][] = [
//          'method' => 'DELETE',
//          // 'method' => 'GET',
//          'uri' => $item['href']
//      ];
//      //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
//    }
//    // TORLES
//    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
//        print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
//    }
//  } while (!is_null($result['next']));
//}
//
//echo "Cleaning done ".date('Y-m-d H:i:s');