<?php

ini_set("memory_limit", "-1");
set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once('loader.php');
require_once('settings.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');


$db = \db\Database::instance();
$query = "SELECT * FROM products AS p " .
        "INNER JOIN products_lang AS pl ON pl.id_prod = p.id_prod AND lang = 'hu'";
//        . "WHERE pl.id_lang = 6 ";
$products = $db->query($query);

$categoryUrls = array();
foreach ($products as $product) {
    $productId = $product["id_prod"];

    $query = "SELECT url FROM seo_url AS s WHERE s.mod = 'P' and item_id = " . $productId . " AND lang = 'hu'";
    $url = $db->findOneByQuery($query, "url");

    if ($url != "") {
        $oldUrl = str_replace(SHOP_URL, "", $url);
        $newUrl = str_replace(".html", "", $oldUrl);
        $arr = explode("/", $url);

        $newUrl = $arr[count($arr) - 1];
        $newUrl = str_replace(".html", "", $newUrl);
        $categoryUrls[] = array(
            "oldUrl" => $oldUrl,
            "newUrl" => $newUrl,
            "order" => 0
        );
    }
}


$fp = fopen('data/szepsegdepoProductUrl.csv', 'w');

$headers = ['old_url', 'new_url', 'order'];
fputcsv($fp, $headers, ";");
foreach ($categoryUrls as $fields) {
    fputcsv($fp, array_values($fields), ";");
}
fclose($fp);

$oldURLlink = "http://szepsegdepo.hu/";
$newURLlink = "https://szepsegdepo.shoprenter.hu/";

foreach ($categoryUrls as $value) {
    echo "<a href='" . $oldURLlink . $value["oldUrl"] . "' target='_blank'>" . $oldURLlink . $value["oldUrl"] . "</a> || "
    . "<a href='" . $newURLlink . $value["newUrl"] . "' target='_blank'>" . $newURLlink . $value["newUrl"] . "</a> || ";
    echo '<br>';
}


