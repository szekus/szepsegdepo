<?php

function pre_print($printable) {
    echo '<pre>';
    print_r($printable);
    echo '</pre>';
}

function slug($string, $space = "-") {
    // $string = utf8_encode($string);

    /*
      if (function_exists('iconv')) {
      $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
      } */
    $string = str_replace(['Á', 'É', 'Í', 'Ő', 'Ú', 'Ó', 'Ö', 'Ű', 'Ü', 'í', 'ü', 'ó', 'ő', 'ú', 'á', 'é', 'ű'], ['A', 'E', 'I', 'O', 'U', 'O', 'O', 'U', 'U', 'i', 'u', 'o', 'o', 'u', 'a', 'e', 'u'], $string);
    $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
    $string = preg_replace('/-/', ' ', $string);
    $string = trim(preg_replace("/\\s+/", " ", $string));
    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);

    return $string;
}

function bolthelyOptionsExplode($optionsRow) {
    $options = [];
    $propparts = explode('*', $optionsRow);
    if (is_array($propparts) && count($propparts) > 0) {
        $propname = array_shift($propparts);
        foreach ($propparts as $prop) {
            $options[$propname][$prop] = $prop; // 1 prop egyszer
        }
    }
    return $options;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function split_name($string) {
    $arr = explode(' ', $string);
    $count = count($arr);
    $firsName = "";
    $lastName = "";
    if ($count == 2) {
        $firsName = $arr[0];
        $lastName = $arr[1];
    } else {
        $firsName = $arr[0];
        for ($i = 1; $i < $count; $i++) {
            $lastName .= $arr[$i] . " ";
        }
    }

    $lastName = trim($lastName);
    if ($lastName == "") {
        $lastName = "_";
    }
    if ($firsName == "") {
        $firsName = "_";
    }
    return array("firstname" => $firsName, "lastname" => $lastName);
}

function getNetPrice($price, $tax) {
    if ($price == "") {
        $price = 0;
    }
    return $price / (1.00 + $tax / 100);
}

function sout($param) {
    echo "\n";
    var_dump($param);
    echo "\n";
}

function getId($url) {
    $arr = explode("/", $url);
    return $arr[count($arr) - 1];
}

function getParamValue($url, $param) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    return $query[$param];
}

function removeAccent($string) {
    $from = array("Á", "É", "Í", "Ó", "Ö", "Ő", "Ú", "Ü", "Ű", "Ó", "O", "U", "Ü", "á", "é", "í", "ó", "ö", "ő", "ú", "ü", "ű", "ó");
    $to = array("A", "E", "I", "O", "O", "O", "U", "U", "U", "O", "O", "U", "U", "a", "e", "i", "o", "o", "o", "u", "u", "u", "o");
    $string = str_replace($from, $to, $string);
    return $string;
}

function createArrayFromCSV($fileUrl) {
    $array = array_map('str_getcsv', file($fileUrl));
    $header = explode(";", $array[0][0]);
//    sout($array);
    $result = array();
    for ($i = 1; $i < count($array); $i++) {
        $data = array();
        $tmp = explode(";", $array[$i][0]);
//        sout($tmp);
//        die();
        for ($j = 0; $j < (count($header)); $j++) {
            $data[$header[$j]] = $tmp[$j];
        }
        $result[] = $data;
    }

    return $result;
}

function getApiEndpointFromHref($href) {
    $arr = explode("/", $href);
    return $arr[count($arr) - 1];
}

function checkAndReturnValue($array, $key) {
    if (array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        return returnDefaultValue($key);
    }
}

function returnDefaultValue($key) {
    switch ($key) {
        case "billing_country":
        case "shipping_country":
            return "HU";
        case "billing_phone":
            return 1;
        default:
            return "";
    }
}
