<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of Attribute
 *
 * @author szekus
 */
class Attribute extends Helper {

    protected $SRId;
    protected $nev;
    protected $cimke;
    protected $attributeValues;
    protected $attributes;
    protected $productClassArray;

    function setProductClassArray($productClassArray) {
        $this->productClassArray = $productClassArray;
    }

    function getAttributes() {
        return $this->attributes;
    }

    function getCimke() {
        return $this->cimke;
    }

    function getProductClassArray() {
        return $this->productClassArray;
    }

    function getAttributeValues() {
        return $this->attributeValues;
    }

    function getNev() {
        return $this->nev;
    }

    public static function create() {
        $attribute = new Attribute();
        return $attribute;
    }

    private function __construct() {
        parent::__construct();
    }

    public function setUp() {
        $this->attributes = array();

        $oneAttribute = Attribute::create();
        $oneAttribute->id = 1;
        $oneAttribute->nev = "Kiszerelés";
        $oneAttribute->cimke = "kiszereles";

        $query = "SELECT id_pl, kiszer, count(*) AS c FROM products_lang WHERE kiszer !='' AND lang = 'hu' GROUP BY kiszer HAVING c > 0";
        $result = $this->db->query($query);

//        sout($res);

        $attributeValueArray = array();
        foreach ($result as $item) {

            $attributeValue = AttributeValue::create();
            $attributeValue->name = $item["kiszer"];
            $attributeValue->id = $item["id_pl"];


            $query = "SELECT id_prod FROM products_lang WHERE lang = 'hu' AND kiszer = '" . $item["kiszer"] . "'";
            $res = $this->db->query($query);

            foreach ($res as $resValue) {
                $attributeValue->productIdArray[] = $resValue["id_prod"];
            }


            $attributeValueArray[] = $attributeValue;
        }
        $oneAttribute->attributeValues = $attributeValueArray;
        $this->attributes[] = $oneAttribute;

        $query = "SELECT * FROM groups_lang WHERE lang = 'hu' AND id_group != 7 ";
        $groups = $this->db->query($query);

        foreach ($groups as $group) {
            $oneAttribute = Attribute::create();
            $oneAttribute->id = $group["id_gl"];
            $oneAttribute->nev = $group["title"];
            $oneAttribute->cimke = slug($group["title"]);

            $query = "SELECT * FROM groups_items_lang AS gil " .
                    "INNER JOIN groups_items AS gi ON "
                    . "gil.id_item = gi.id_item AND "
                    . "lang = 'hu' AND "
                    . "gi.id_group =  " . $group["id_group"];
//            sout($query);
            $group_items = $this->db->query($query);


            $attributeValueArray = array();
            foreach ($group_items as $group_item) {
                $attributeValue = AttributeValue::create();
                $attributeValue->name = $group_item["title"];
                $attributeValue->id = $group_item["id_item"];

                $query = "SELECT id_prod FROM products_group WHERE id_item = " . $group_item["id_item"];
                $res = $this->db->query($query);

                foreach ($res as $resValue) {
                    $attributeValue->productIdArray[] = $resValue["id_prod"];
                }

                $attributeValueArray[] = $attributeValue;
            }
            $oneAttribute->attributeValues = $attributeValueArray;
            $this->attributes[] = $oneAttribute;
        }

        foreach ($this->attributes as $attr) {

            if ($attr->cimke == "kiszereles") {
                $productClassIdArray = array();
                foreach ($attr->attributeValues as $attrValue) {
                    $query = "SELECT some FROM products WHERE id_prod IN (SELECT id_prod FROM products_lang WHERE lang = 'hu' AND kiszer = '" . $attrValue->name . "')";

                    $productClassIdArray = $this->db->query($query);

                    foreach ($this->productClassArray as &$productClass) {
                        foreach ($productClassIdArray as $productClassId) {
                            if ($productClass["original_id"] == $productClassId["some"] && $productClassId["some"] != "0") {
                                if (array_key_exists("first", $productClass) && !array_key_exists("second", $productClass)) {
                                    if ($attr->cimke != $productClass["first"]) {
                                        $productClass["second"] = $attr->cimke;
                                    }
                                } else if (!array_key_exists("first", $productClass)) {
                                    $productClass["first"] = $attr->cimke;
                                }
                            }
                        }
                    }
                }
            } else {
                $productClassIdArray = array();
                foreach ($attr->attributeValues as $attrValue) {

                    $query = "SELECT some FROM products WHERE id_prod IN (SELECT id_prod FROM products_group WHERE id_item = " . $attrValue->id . ") GROUP BY some";

                    $productClassIdArray = $this->db->query($query);

                    foreach ($this->productClassArray as &$productClass) {
                        foreach ($productClassIdArray as $productClassId) {
                            if ($productClass["original_id"] == $productClassId["some"]) {

                                if (array_key_exists("first", $productClass) && !array_key_exists("second", $productClass)) {
                                    if ($attr->cimke != $productClass["first"]) {
                                        $productClass["second"] = $attr->cimke;
                                    }
                                } else if (!array_key_exists("first", $productClass)) {
                                    $productClass["first"] = $attr->cimke;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
