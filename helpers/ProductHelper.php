<?php

namespace helpers;

define('RAKTARON', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTk=');
define('KESZLETHIANY', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTEz');
define('SZALLITAS_1_NAP', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE0');
define('SZALLITAS_TIZ_NAP', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE1');
define('SZALLITAS_24_ORA', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE2');
define('WEIGHT_CLASS_G', 'd2VpZ2h0Q2xhc3Mtd2VpZ2h0X2NsYXNzX2lkPTI=');
define('PARENT_ID_NUMBER_ADD', 10000);

use resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductHelper
 *
 * @author szekus
 */
class ProductHelper extends Helper {

    protected $products;
    protected $productClass;
    protected $childProducts;
    public $parentProducts;

    public function __construct() {
        parent::__construct();
        $this->limit = -1;
    }

    function getProducts() {
        return $this->products;
    }

    function setProducts($products) {
        $this->products = $products;
    }

    function getChildProducts() {
        return $this->childProducts;
    }

    function getProductClass() {
        return $this->productClass;
    }

    function setParentProducts() {
        $query = "SELECT * FROM some_codes";
        $some_codes = $this->db->query($query);

        foreach ($some_codes as $value) {

            $parentId = PARENT_ID_NUMBER_ADD + intval($value["id"]);

            $sku = "productGroup-" . $parentId;
            $productResource = \resources\Product::create();
            $productSRId = $productResource->createSRId($parentId);

            $productResource->sku = $sku;
            $productResource->id = $productSRId;
            $productResource->innerId = $parentId;
            $this->parentProducts[] = $productResource;
//            
//            
//            $this->parentProducts[] = $productResource->getBySRId($sku);
        }
    }

    function setProductClass() {
        $query = "SELECT * FROM aaasr_productclasses";
        $result = $this->db->query($query);
        $array = array();
        foreach ($result as $value) {
            $array[] = $value;
        }

        for ($i = 0; $i < count($array); $i++) {
            $class = array();
            $class = $array[$i];

            $name = $array[$i]["name"];
            $arr = explode("-", $name);
            $original_id = trim($arr[count($arr) - 1]);
            $class["original_id"] = $original_id;

            $this->productClass[] = $class;
        }
    }

    function setChildProducts($childProducts) {
        foreach ($products as $child) {
            $childProductSRId = base64_encode("product-product_id=" . $child["id_product"]);
            $productResource = resources\Product::create();
            $child = $productResource->getBySRId($childProductSRId);
            $this->childProducts[] = $child;
        }
    }

    public function createParentProducts() {
        $query = "SELECT * FROM some_codes";
        $parentProducts = $this->db->query($query);
        foreach ($parentProducts as $parent) {
            $productResource = \resources\Product::create();



            $productId = intval($parent["id"]);
            $productId = PARENT_ID_NUMBER_ADD + $productId;
            $productResource->id = $productId;
            $productResource->innerId = $productId;
            $productResource->sku = "productGroup-" . $productId;

            $productResource->stock1 = 100;
            $productResource->orderable = 1;
            $productResource->subtractStock = 1;
            $productResource->status = 1;
            $productResource->price = 1;

            $productResource->taxClass = array("id" => TAX_27_CLASS);

            $productResource->insert(true);

            $productSRId = $productResource->createSRId($productId);

            $query = "SELECT id_categ FROM products_categ " .
                    "WHERE id_prod IN (" .
                    "SELECT p.id_prod FROM products AS p WHERE p.some = " . $parent["id"] . ") " .
                    "GROUP BY id_categ";

            $categories = $this->db->query($query);

            foreach ($categories as $cat) {
                $categoryId = $cat["id_categ"];

                $categorySRId = base64_encode("category-category_id=" . $categoryId);
                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

                sout($productCategoryRelationResource->getData());
                $productCategoryRelationResource->insert(true);
//                $this->request->addBatch($productCategoryRelationResource->getData());
            }


            $productDescritionResource = resources\ProductDescription::create();
            $productDescritionResource->name = $parent["title"];
            $productDescritionResource->metaKeywords = "";
            $productDescritionResource->metaDescription = "";
            $productDescritionResource->shortDescription = "";
            $productDescritionResource->description = "";
            $productDescritionResource->product = array("id" => $productSRId);
            $productDescritionResource->language = array("id" => LANG_HU);

            $productDescritionResource->insert(true);
//            $this->request->addBatch($productDescritionResource->getData());
        }
    }

    public function createChildProduct() {
        foreach ($this->products as $product) {

            $some = $product["some"];

            if ($some != "0" && $some != 0) {
                $productResource = new resources\Product();

                $childId = $product["id_prod"];
                $childProductSRId = $productResource->createSRId($childId);

                $parentId = PARENT_ID_NUMBER_ADD + intval($some);
                $parentSku = "productGroup-" . $parentId;

//                $productParentSRId = $productResource->getProductIdBySku($parentSku);
                $productParentSRId = $productResource->createSRId($parentId);

                $productResource->id = $childProductSRId;
                $productResource->parentProduct = array("id" => $productParentSRId);

                $productResource->update(true);
            }
        }
    }

    public function insertProducts() {

        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productResource->id = $productId;
            $productResource->innerId = $productId;

            $sku = $product["kod"] . "-" . $product["id_prod"];
            if ($product["kod"] == "") {
                $sku = $product["id_prod"] . "-" . $product["id_prod"];
            }
            $productResource->sku = $sku;
            $productResource->stock1 = $product["raktar"];
            $productResource->stock2 = $product["raktar_act"];

            $rendelheto = 1;
            if ($product["rendelheto"] != "Y") {
                $rendelheto = 0;
            }
            $productResource->orderable = $rendelheto;
            $productResource->subtractStock = 1;


            $productResource->weight = $product["suly"];
            $productResource->weightUnit = array("id" => WEIGHT_CLASS_G);

            $status = 1;
            if ($product["status"] != "Y") {
                $status = 0;
            }
            $productResource->status = $status;


            $productResource->price = getNetPrice($product["prod_price"], $product["fee"]);

            $taxClass = TAX_27_CLASS;
            if ($product["fee"] == 24) {
                $taxClass = TAX_24_CLASS;
            }
            $productResource->taxClass = array("id" => $taxClass);
//            $productResource->availableDate = $product["Product Published"];
//            
            $query = "SELECT * FROM products_group WHERE id_prod =  " . $productId
                    . "AND id_item IN "
                    . "(SELECT id_item FROM groups_items_lang "
                    . "WHERE lang = 'hu' AND "
                    . "id_item IN "
                    . "(SELECT id_item FROM groups_items WHERE id_group = 7))";
            $res = $this->db->query($query);
//            
            if (count($res) > 0) {
                $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $res["id_item"]);
                $productResource->manufacturer = array("id" => $manufacturerSRId);
            }

            $query = "SELECT image FROM products_img WHERE status = 'Y' AND id_prod = " . $product["id_prod"] .
                    " UNION " .
                    " SELECT image FROM products_img2 WHERE id_prod = " . $product["id_prod"];

            $images = $this->db->query($query);

            foreach ($images as $img) {
                $productResource->mainPicture = "prod/" . $img["image"];
                $productResource->mageAlt = "";
                break;
            }


//            sout($productResource->getData());
            $productResource->insert(true);



            $query = "SELECT * FROM products_categ WHERE id_prod = " . $product["id_prod"];
            $categories = $this->db->query($query);
            foreach ($categories as $cat) {
                $categoryId = $cat["id_categ"];

                $categorySRId = base64_encode("category-category_id=" . $categoryId);
                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

//                sout($productCategoryRelationResource->getData());
                $productCategoryRelationResource->insert(true);
            }

            if ($this->counter == $this->limit) {
                die();
            }
        }
    }

    public function updateProductCategory() {
        foreach ($this->products as $product) {
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT * FROM products_categ WHERE id_prod = " . $product["id_prod"];
            $categories = $this->db->query($query);
            foreach ($categories as $cat) {
                $categoryId = $cat["id_categ"];

                $categorySRId = base64_encode("category-category_id=" . $categoryId);
                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

//                sout($productCategoryRelationResource->getData());
                $productCategoryRelationResource->insert(true);
                $this->request->addBatch($productCategoryRelationResource->createBatchArray());
            }
        }
    }

    public function insertProductDescription() {
        foreach ($this->products as $product) {
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productDescritionResource = resources\ProductDescription::create();
            $productDescritionResource->name = $product["title"];
            $productDescritionResource->metaKeywords = $product["meta_keyw"];
            $productDescritionResource->metaDescription = $product["meta_desc"];
            $productDescritionResource->shortDescription = "";




            $productDescritionResource->description = $product["body"];
            $productDescritionResource->product = array("id" => $productSRId);
            $productDescritionResource->language = array("id" => LANG_HU);

            $productDescritionResource->insert(true);
        }
    }

    public function insertProductsImage() {
        foreach ($this->products as $product) {


            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT image FROM products_img WHERE id_prod = " . $product["id_prod"] .
                    " UNION " .
                    " SELECT image FROM products_img2 WHERE id_prod = " . $product["id_prod"];

            $images = $this->db->query($query);
            for ($i = 1; $i < count($images); $i++) {
                $img = $images[$i];
                $productImageResource = resources\ProductImage::create();
                $productImageResource->imagePath = "prod/" . $img["image"];
                $productImageResource->imageAlt = "";
                $productImageResource->product = array("id" => $productSRId);

                $productImageResource->insert();
            }
        }
    }

    public function inserProductSpecial() {
        foreach ($this->products as $product) {

            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $isSpecial = false;

            if ($product["prod_price2"] != "0" && $product["prod_price2"] != 0) {

                $productSpecialResource = \resources\ProductSpecial::create();
                $price = getNetPrice($product["prod_price2"], $product["fee"]);
                $productSpecialResource->price = $price;

                $productSpecialResource->product = array("id" => $productSRId);
                $productSpecialResource->insert();
                if (!$isSpecial) {
                    $isSpecial = true;
                }
            }


            if ($product["disc_val"] != "0" && $product["disc_val"] != 0) {

                $productSpecialResource = \resources\ProductSpecial::create();
                $price = getNetPrice($product["disc_val"], $product["fee"]);
                $productSpecialResource->price = $price;

                $productSpecialResource->product = array("id" => $productSRId);

                if ($product["disc_status"] != "N") {
                    $productSpecialResource->dateFrom = $product["disc_start"];
                    $productSpecialResource->dateTo = $product["disc_end"];
                }
                $productSpecialResource->insert();
                if (!$isSpecial) {
                    $isSpecial = true;
                }
            }
            if ($product["disc_per"] != "0" && $product["disc_per"] != 0) {

                $productSpecialResource = \resources\ProductSpecial::create();

                $price = getNetPrice($product["prod_price"], $product["fee"]);
                if ($product["discount"] != "0" && $product["discount"] != 0) {
                    $price = getNetPrice($price, $product["discount"]);
                }
                if ($product["price"] != "0" && $product["price"] != 0) {
                    $price = getNetPrice($product["price"], $product["fee"]);
                }

                $productSpecialResource->price = $price;

                $productSpecialResource->product = array("id" => $productSRId);
                $productSpecialResource->insert();
                if (!$isSpecial) {
                    $isSpecial = true;
                }
            }


            $query = "SELECT * FROM products AS p " .
                    "INNER JOIN va_prods AS vp ON p.id_prod = vp.id_prod AND p.id_prod = " . $productId;
            $va_prods = $this->db->query($query);


            foreach ($va_prods as $product) {
                $productSpecialResource = \resources\ProductSpecial::create();


                if ($product["discount"] != "0" && $product["discount"] != 0) {
                    $price = getNetPrice($product["prod_price"], $product["fee"]);
                    $price = getNetPrice($price, $product["discount"]);
                }

                if ($product["discount"] == "0" && $product["discount"] == 0) {
                    $price = getNetPrice($product["price"], $product["fee"]);
                }


                $productSpecialResource->price = $price;

                $productSpecialResource->product = array("id" => $productSRId);

                $customerGroup = SR_VASARLOI_CSOPORT_1;

                switch ($product["id_va"]) {
                    case "1":
                        $customerGroup = SR_VASARLOI_CSOPORT_1;
                        break;
                    case "2":
                        $customerGroup = SR_VASARLOI_CSOPORT_2;
                        break;
                    case "3":
                        $customerGroup = SR_VASARLOI_CSOPORT_3;
                        break;
                }
                $productSpecialResource->customerGroup = array("id" => $customerGroup);

                $productSpecialResource->insert();
                if (!$isSpecial) {
                    $isSpecial = true;
                }
            }

            if ($isSpecial) {
                $this->addBadge($productSRId);
            }
        }
    }

    public function addBadge($productSRId) {
        $productProductBadgeRelatinResource = \resources\ProductProductBadgeRelation::create();
        $productProductBadgeRelatinResource->product = array("id" => $productSRId);
        $productProductBadgeRelatinResource->productBadge = array("id" => PRODUCT_BADGE);
        $productProductBadgeRelatinResource->language = array("id" => LANG_HU);
        $productProductBadgeRelatinResource->insert();
    }

    public function createProductClass() {
        $query = "SELECT * FROM some_codes";
        $parentProducts = $this->db->query($query);

        foreach ($parentProducts as $parentProductValue) {


            $productId = intval($parentProductValue["id"]);
            $productId = PARENT_ID_NUMBER_ADD + $productId;
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productClassResource = \resources\ProductClass::create();
            $productClassResource->name = $parentProductValue["title"] . " - " . $parentProductValue["id"];
            $productClassResource->description = "";


            $productClassResult = $productClassResource->insert(true);

            $productResource = resources\Product::create();

            $sku = "productGroup-" . $productId;
            $productResource->sku = $sku;
            $productResource->productClass = array("id" => $productClassResult["id"]);

//            $productSRId = $productResource->getProductIdBySku($sku);
            $productResource->id = $productSRId;
            $productResource->update(true);

            $productResource = resources\Product::create();
            $productResource->productClass = array("id" => $productClassResult["id"]);
            $productResource->parentProduct = array("id" => $productSRId);


            foreach ($this->products as $child) {
                if ($child["some"] == $parentProductValue["id"]) {
                    $childSRId = $productResource->createSRId($child["id_prod"]);
                    $sku = $child["kod"] . "-" . $child["id_prod"];
                    if ($child["kod"] == "") {
                        $sku = $child["id_prod"] . "-" . $child["id_prod"];
                    }
                    $productResource->sku = $sku;
                    $productResource->id = $childSRId;
                    $productResource->update(true);
                }
            }
        }
    }

//    public function insertProductClass() {
//
//        $query = "SELECT parent_id, COUNT(*) c FROM catalog_product_relation GROUP BY parent_id HAVING c > 1";
//        $result = $this->db->query($query);
//
//        foreach ($result as $value) {
//            $productClass = array();
//            $srProduct = array();
//            $productClass["id"] = $value["parent_id"];
//            $productId = $value["parent_id"];
//            $productSRId = base64_encode("product-product_id=" . $productId);
//
//            $query = "SELECT value FROM catalog_product_entity_varchar WHERE entity_id = " . $productId . " AND attribute_id = 71";
//            $name = $this->db->findOneByQuery($query, "value");
//            if ($name != NULL) {
//                $productClass["name"] = $name . " - " . $value["parent_id"];
//            } else {
//                $productClass["name"] = $value["parent_id"];
//            }
//
////            $query = "SELECT value FROM catalog_product_entity_text WHERE entity_id = " . $productId . " AND attribute_id = 72";
////            $description = $this->db->findOneByQuery($query, "value");
////            if ($description != NULL) {
////                $description .= $this->getMoreInformation($productId);
////                $productClass["description"] = $description;
////            }
//
//            $productClass["description"] = "";
//            $productClassResult = querySRApi("/productClasses", $productClass, 'POST', "responseBody", false);
//
//            $srProduct["productClass"] = array("id" => $productClassResult["id"]);
//            $skuQuery = "SELECT sku FROM catalog_product_entity WHERE entity_id = " . $productId;
//            $sku = $this->db->findOneByQuery($skuQuery, "sku");
//            $srProduct["sku"] = $sku;
//            $sr_product = querySRApi("/products/" . $productSRId, $srProduct, 'POST', "responseBody", true);
//
////            sout($productClassResult);
//            $query = "SELECT * FROM catalog_product_relation WHERE parent_id = " . $value["parent_id"];
//            $res = $this->db->query($query);
//            if ($res != FALSE) {
//                foreach ($res as $children) {
//                    $productSRId = base64_encode("product-product_id=" . $children["child_id"]);
//
//                    $srProduct = array();
//
//                    $srProduct["parentProduct"] = array("id" => base64_encode("product-product_id=" . $children["parent_id"]));
//                    $srProduct["productClass"] = array("id" => $productClassResult["id"]);
//
//                    $skuQuery = "SELECT sku FROM catalog_product_entity WHERE entity_id = " . $children["child_id"];
//                    $sku = $this->db->findOneByQuery($skuQuery, "sku");
//                    $srProduct["sku"] = $sku;
//
//                    $sr_product = querySRApi("/products/" . $productSRId, $srProduct, 'POST', "responseBody", TRUE);
//                }
//            }
//        }
//    }

    public function createProductAttribute($attributeHelper) {

        $attributes = $attributeHelper->getAttributes();

        for ($i = 0; $i < count($attributes); $i++) {
            $attribute = $attributes[$i];

            $listAttributeResource = resources\ListAttribute::create();
            $listAttributeResource->type = "LIST";
            $listAttributeResource->name = $attribute->getCimke();
            $listAttributeResource->priority = "NORMAL";
            $listAttributeResource->sortOrder = "NORMAL";
            $listAttributeResource->required = 0;
            $listAttributeResource->presentation = "TEXT";

            $listAttributeResult = $listAttributeResource->insert(true);

            $attributeDescriptionResource = \resources\AttributeDescription::create();


            $attributeDescriptionResource->name = $attribute->getNev();
            $attributeDescriptionResource->description = "";
            $attributeDescriptionResource->attribute = array('id' => $listAttributeResult["id"]);
            $attributeDescriptionResource->language = array('id' => LANG_HU);
            $attributeDescriptionResource->insert(true);

            foreach ($this->productClass as $productClass) {
                $productClassAttributeRelationResource = \resources\ProductClassAttributeRelation::create();
                $productClassAttributeRelationResource->productClass = array('id' => $productClass["id"]);
                $productClassAttributeRelationResource->attribute = array('id' => $listAttributeResult["id"]);
                $productClassAttributeRelationResource->insert(true);
            }


            foreach ($attributeHelper->getProductClassArray() as $productClass) {

                if (array_key_exists("first", $productClass)) {
                    if ($attribute->getCimke() == $productClass["first"]) {
                        $productClassResource = \resources\ProductClass::create();
                        $productClassResource->id = $productClass["id"];
                        $productClassResource->firstVariantSelectType = "SELECT";
                        $productClassResource->firstVariantParameter = array("id" => $listAttributeResult["id"]);
                        $productClassResource->update(true);
                    }
                }
                if (array_key_exists("second", $productClass)) {
                    if ($attribute->getCimke() == $productClass["second"]) {
                        $productClassResource = \resources\ProductClass::create();
                        $productClassResource->id = $productClass["id"];
                        $productClassResource->secondVariantSelectType = "SELECT";
                        $productClassResource->secondVariantParameter = array("id" => $listAttributeResult["id"]);
                        $productClassResource->update(true);
                    }
                }
            }

            foreach ($attribute->getAttributeValues() as $attributeValue) {
                $name = $attributeValue->name;

                $listAttributeValueResource = \resources\ListAttributeValue::create();
                $listAttributeValueResource->listAttribute = array('id' => $listAttributeResult['id']);

                $resultAV = $listAttributeValueResource->insert(true);

                $listAttributeValueDescriptionResource = \resources\ListAttributeValueDescription::create();
                $listAttributeValueDescriptionResource->listAttributeValue = array('id' => $resultAV["id"]);
                $listAttributeValueDescriptionResource->language = array('id' => LANG_HU);
                $listAttributeValueDescriptionResource->name = $name;

                $resultAVD = $listAttributeValueDescriptionResource->insert(true);

                foreach ($attributeValue->productIdArray as $childId) {
                    $productSRId = base64_encode("product-product_id=" . $childId);
                    $productListAttributeValueRelationResource = \resources\ProductListAttributeValueRelation::create();
                    $productListAttributeValueRelationResource->product = array("id" => $productSRId);
                    $productListAttributeValueRelationResource->listAttributeValue = array("id" => $resultAV["id"]);
                    sout($productListAttributeValueRelationResource->getData());
                    $productClassAttributeRelationsResult = $productListAttributeValueRelationResource->insert(true);
                }
            }
        }
    }

    public function getProductClasses() {
//        $result = querySRApi("/productClasses?limit=200", [], 'GET', "responseBody", FALSE);
//        $array = [];
//        foreach ($result["items"] as $item) {
//            $id = getId($item["href"]);
//            $res = querySRApi("/productClasses/" . $id, [], 'GET', "responseBody", FALSE);
//            if (key_exists("name", $res)) {
//                $name = $res["name"];
//                $arr = explode("-", $name);
//                $original_id = $arr[count($arr) - 1];
//                $productClass = [];
//
//                $productClass["original_id"] = $original_id;
//                $productClass["id"] = $res["id"];
//                $productClass["name"] = $res["name"];
//                $array[] = $productClass;
//            }
//        }
//
//        return ($array);
    }

//    public function createProductAttribute() {
//
//        $valtozatok = array();
//
//        $query = "SELECT * FROM groups_lang WHERE lang = 'hu' AND id_group != 7 ";
//        $groups = $this->db->query($query);
//
//        foreach ($groups as $value) {
//            $egyValtozat = array();
//            $egyValtozat["nev"] = $value["title"];
//            $egyValtozat["cimke"] = slug($value["title"]);
//
//            $query = "SELECT * FROM groups_items_lang AS gil " .
//                    "INNER JOIN groups_items AS gi ON "
//                    . "gil.id_item = gi.id_item AND "
//                    . "lang = 'hu' AND "
//                    . "gi.id_group =  " . $value["id_group"];
//            $group_items = $this->db->query($query);
//
//
//            $ertek = array();
//            foreach ($group_items as $group_item) {
//                $arr = array();
//                $arr["name"] = $group_item["title"];
//                $arr["id_item"] = $group_item["id_item"];
//                $ertek[] = $arr;
//            }
//            $egyValtozat["ertekek"] = $ertek;
//            $valtozatok[] = $egyValtozat;
//        }
//
//
//        $egyValtozat = array();
//        $egyValtozat["nev"] = "Kiszerelés";
//        $egyValtozat["cimke"] = "kiszereles";
//
//        $query = "SELECT id_pl, kiszer, count(*) AS c FROM products_lang WHERE kiszer !='' GROUP BY kiszer HAVING c > 0";
//        $res = $this->db->query($query);
//
//
//        $ertek = array();
//        foreach ($res as $item) {
//            $arr = array();
//            $arr["name"] = $item["kiszer"];
//            $arr["id_pl"] = $item["id_pl"];
//            $ertek[] = $arr;
//        }
//        $egyValtozat["ertekek"] = $ertek;
//        $valtozatok[] = $egyValtozat;
//
//
//        for ($i = 0; $i < count($valtozatok); $i++) {
//            $listAttribute = [];
//            $listAttribute["type"] = "LIST";
//            $listAttribute["name"] = $valtozatok[$i]["cimke"];
//            $listAttribute["priority"] = "NORMAL";
//            $listAttribute["sortOrder"] = "NORMAL";
//            $listAttribute["required"] = 0;
//            $listAttribute["presentation"] = "TEXT";
//
//            $listAttributeResult = querySRApi("/listAttributes", $listAttribute, 'POST', "responseBody", true);
//
//            $attributeDesc = Array
//                (
//                'name' => $valtozatok[$i]["nev"],
//                'description' => "",
//                'attribute' => array('id' => $listAttributeResult["id"]),
//                'language' => array('id' => LANG_HU)
//            );
//
//            $attributeDescResult = querySRApi("/attributeDescriptions", $attributeDesc, 'POST', "responseBody", true);
//
//
//            foreach ($this->parentProducts as $parent) {
//                $parentId = intval($parent->innerId) - PARENT_ID_NUMBER_ADD;
//                foreach ($this->productClass as $productClass) {
//                    if ($productClass["original_id"] == $parentId) {
//                        $productClassAttributeRelations = Array
//                            (
//                            'productClass' => array('id' => $productClass["id"]),
//                            'attribute' => array('id' => $listAttributeResult["id"])
//                        );
//                        $productClassAttributeRelationsResult = querySRApi("/productClassAttributeRelations", $productClassAttributeRelations, 'POST', "responseBody", true);
//
//                        $productClassResource = \resources\ProductClass::create();
//                        $productClassResource->id = $productClass["id"];
//                        $productClassResource->firstVariantSelectType = "SELECT";
//                        $productClassResource->firstVariantParameter = array("id" => $listAttributeResult["id"]);
//                        $productClassResource->update(true);
//                    }
//                }
//            }
//
//            foreach ($valtozatok[$i]["ertekek"] as $ertekek) {
//                $name = $ertekek["name"];
//
//
//                $req = Array
//                    (
//                    'listAttribute' => array('id' => $listAttributeResult['id'])
//                );
//                $resultAV = querySRApi("/listAttributeValues", $req, 'POST', "responseBody", true);
//
//                $req = Array
//                    (
//                    'listAttributeValue' => array('id' => $resultAV["id"]),
//                    'language' => array('id' => LANG_HU),
//                    'name' => $name
//                );
//                $resultAVD = querySRApi("/listAttributeValueDescriptions", $req, 'POST', "responseBody", true);
//
//
//                if ($valtozatok[$i]["cimke"] != "kiszereles") {
//                    $query = "SELECT * FROM products_group WHERE id_item = " . $ertekek["id_item"];
//                } else {
//                    $query = "SELECT * FROM products_lang WHERE kiszer = '" . $name . "'";
//                }
//
//                $resultQueryOption = $this->db->query($query);
//
//                foreach ($resultQueryOption as $option) {
//                    foreach ($this->products as $child) {
//                        if ($option["id_prod"] == $child["id_prod"]) {
//                            $productSRId = base64_encode("product-product_id=" . $child["id_prod"]);
//                            $productListAttributeValueRelations = [];
//                            $productListAttributeValueRelations["product"] = ["id" => $productSRId];
//                            $productListAttributeValueRelations["listAttributeValue"] = ["id" => $resultAV["id"]];
//                            $productClassAttributeRelationsResult = querySRApi("/productListAttributeValueRelations", $productListAttributeValueRelations, 'POST', "responseBody", false);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public function getProductClasses() {
////        $result = querySRApi("/productClasses?limit=200", [], 'GET', "responseBody", FALSE);
////        $array = [];
////        foreach ($result["items"] as $item) {
////            $id = getId($item["href"]);
////            $res = querySRApi("/productClasses/" . $id, [], 'GET', "responseBody", FALSE);
////            if (key_exists("name", $res)) {
////                $name = $res["name"];
////                $arr = explode("-", $name);
////                $original_id = $arr[count($arr) - 1];
////                $productClass = [];
////
////                $productClass["original_id"] = $original_id;
////                $productClass["id"] = $res["id"];
////                $productClass["name"] = $res["name"];
////                $array[] = $productClass;
////            }
////        }
////
////        return ($array);
//    }

    public function insertRelatedProductRelations() {
        $query = "SELECT * FROM products_product";
        $result = $this->db->query($query);
        foreach ($result as $value) {
            $productRelatedProductRelation = resources\ProductRelatedProductRelation::create();
            $productId = base64_encode("product-product_id=" . $value["id_prod"]);
            $relatedProductId = base64_encode("product-product_id=" . $value["id_prod2"]);
            $productRelatedProductRelation->product = array("id" => $productId);
            $productRelatedProductRelation->relatedProduct = array("id" => $relatedProductId);
            $productRelatedProductRelation->insert(true);
        }
    }

    public function insertProductTag() {
        foreach ($this->products as $product) {
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productTag = \resources\ProductTag::create();
            $tags = "";
            $productTag->product = array("id" => $productSRId);
            $productTag->language = array("id" => LANG_HU);

            $query = "SELECT tl.tag FROM tags_lang AS tl " .
                    "INNER JOIN products_tags AS pt ON " .
                    "pt.id_tag = tl.id_tag AND tl.lang = 'hu' AND pt.id_prod = " . $productId;

            $result = $this->db->query($query);
            foreach ($result as $value) {
                $name = $value["tag"];
                $tags .= $name . ",";
            }
            if ($tags != "") {
                $tags = substr($tags, 0, -1);
                $productTag->tags = $tags;
                $productTag->insert(true);
            }
        }
    }

    public function insertProductUrlAlias() {
        $urlAlias = \resources\URLAlias::create();
        $urlAlias->deleteProductUrl();
        foreach ($this->products as $product) {
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT url FROM seo_url AS s WHERE s.mod = 'P' and item_id = " . $productId . " AND lang = 'hu'";
            $url = $this->db->findOneByQuery($query, "url");

            if ($url != "") {
                $url = str_replace(SHOP_URL, "", $url);
                $url = str_replace(".html", "", $url);
                $arr = explode("/", $url);

                $url = $arr[count($arr) - 1];
            } else {
                $url = slug($product["title"]) . "-" . $productId;
            }


            $urlAlias->type = "PRODUCT";
            $urlAlias->urlAliasEntity = array("id" => $productSRId);


            $urlAlias->urlAlias = $url;
            $urlAlias->insert(true);
        }
    }

    public function insertCategoryUrlAlias() {
        $urlAliasResource = new \resources\URLAlias();
        $urlAliasResource->deleteCategoryUrl();

        $query = "SELECT * FROM ps_category";
        $categories = $this->db->query($query);
        foreach ($categories as $category) {

            $categoryId = $category["id_category"];
            $categorySRId = base64_encode("category-category_id=" . $categoryId);

            $query = "SELECT link_rewrite FROM ps_category_lang WHERE id_lang = 6 AND id_category = " . $categoryId;
            $link_rewrite = $this->db->findOneByQuery($query, "link_rewrite");
            $urlAlias = $categoryId . "-" . $link_rewrite;

            $urlAliasResource->setType("CATEGORY");
            $urlAliasResource->setUrlAliasEntity(["id" => $categorySRId]);
            $urlAliasResource->setUrlAlias($urlAlias);
            $urlAliasResource->insert();
        }
    }

    public function create301Product() {
        $csvArray = array();
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $url = $productId . "-" . $product["link_rewrite"];
            $csv["old_url"] = $url . ".html";
            $csv["new_url"] = $url;
            $csv["order"] = 0;
            $csvArray[] = $csv;
//            foreach ($this->childProducts as $child) {
//                if ($child["parent_id"] == $productId) {
//                    $parentId = $child["parent_id"];
//                    $urlChild = $parentId . "-" . $child["increment"] . "-" . $product["link_rewrite"];
//                    $csvArray["old_url"] = $urlChild . ".html";
//                    $csvArray["new_url"] = $urlChild;
//                }
//            }
        }
        $headers = ['old_url', 'new_url', 'order'];
        $fp = fopen('data/sutnifozniProductURL.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    private function createImageUrlFromImageId($imgId) {
        $array = str_split($imgId);
        $dir = "p/";
        foreach ($array as $value) {
            $dir .= $value . "/";
        }

        $url = $dir . $imgId . "-watermark.jpg";

        return $url;
    }

    private function removoIfMatch($string, $character) {
        if (substr($string, -1) === $character) {
            return substr($string, 0, -1);
        } else {
            return $string;
        }
    }

}
