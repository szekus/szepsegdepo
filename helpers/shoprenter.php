<?php

DEFINE('QUERY_DEBUG', false);
DEFINE('ONLY_ERROR', true);
DEFINE('IS_LOGGING', false);

// TODO: kell irni egy olyat ami kimondott a batchre van
function querySRApi($path, $arr = [], $method = 'GET', $retType = "responseBody", $debug = false) {
    $apiCall = new ApiCall(SR_USERNAME, SR_APIKEY);
    $apiCall->setFormat('json');
    $response = $apiCall->execute($method, SR_APIURL . $path, $arr);
//    var_dump($response);
    if (IS_LOGGING) {
//        if ($response->getStatusCode() != 200 && $response->getStatusCode() != 201) {
        $logFile = "data/log.txt";
        $current = file_get_contents($logFile);
        $current .= "\n" . "\n" .
                $response->getStatusCode() . "\n" . "\n" .
                SR_APIURL . $path . "\n" . "\n" .
                ($response->getResponseBody()) . "\n" . "\n";

        file_put_contents($logFile, $current);
//        }
    }

    if ($debug && ONLY_ERROR) {
        if ($response->getStatusCode() != 200 && $response->getStatusCode() != 201) {
            print_r($response);
            echo SR_APIURL . $path . '<br>';
        }
    }

    if ($debug && QUERY_DEBUG)
        print_r($response);
    if ($response->getStatusCode() == 409) {
        $response = $apiCall->execute('DELETE', SR_APIURL . $path . $response->getParsedResponseBody()["id"], []);
        $response = $apiCall->execute($method, SR_APIURL . $path, $arr);
    }
    if ($debug && QUERY_DEBUG)
        echo SR_APIURL . $path . '<br>';
    if ($retType == "responseBody")
        return $response->getParsedResponseBody();
    if ($retType == "statusCode")
        return $response->getStatusCode();
}

function createDirectories($path, $removeFile = true) {
    $dirs = explode('/', $path);
    if ($removeFile) {
        unset($dirs[count($dirs) - 1]);
    }

    $fullpath = '';
    foreach ($dirs as $key => $value) {
        $fullpath .= $fullpath == '' ? '' : '/';
        $fullpath .= $value;
        @mkdir($fullpath);
    }
}

function insertManufacturer($data) {
    $postData = array(
        "id" => $data['id'],
        "sortOrder" => isset($data['sortOrder']) ? $data['sortOrder'] : 1,
        "picture" => isset($data['picture']) ? $data['picture'] : '',
        "name" => $data['name'],
    );
    $response = querySRApi(API_ENDPOINT_MANUFACTURER, $postData, "POST", "responseBody", true);

    $data["sr_id"] = $response["id"];
    $data["innerId"] = $response['innerId'];
    return $data;
}

function insertCategories($categories) {
    $i = 0;
    foreach ($categories as $category) {

        $postData = array("id" => $category['id'], "status" => $category['status'], "sortOrder" => $category['sortOrder'], "picture" => $category['picture'], "productsStatus" => $category['productsStatus']);
        if (isset($category['parentCategoryRef']) && isset($categories[$category['parentCategoryRef']])) {
            $parent = $categories[$category['parentCategoryRef']];
            if (isset($parent['sr_id'])) {
                $postData['parentCategory'] = [
                    'id' => $parent['sr_id']
                ];
            }
        }
        $cat['main'] = querySRApi("/categories", $postData, "POST", "responseBody", true);

        echo "updating " . $i . " / " . sizeof($categories) . " updating " . $cat['main']["id"] . "\n";


        $categories[$i]["sr_id"] = $cat['main']["id"];
        $categories[$i]["innerId"] = $cat['main']['innerId'];
        // Updateing category desc
        $catArr = array(
            "name" => $category["name"],
            "language" => array("id" => LANG_HU),
            "metaKeywords" => $categories[$i]['metaKeywords'],
            "metaDescription" => $categories[$i]['metaDescription'],
            "category" => array("id" => $categories[$i]["sr_id"])
        );
        if (isset($categories[$i]['customTitle'])) {
            $catArr['customTitle'] = $categories[$i]['customTitle'];
        }
        $cat['desc'] = querySRApi("/categoryDescriptions", $catArr, "POST", "responseBody", false);

        // Update Ulr aliases
        if ($category["urlAlias"] != '') {
            $urlAlias = [
                'type' => 'CATEGORY',
                'urlAliasEntity' => array("id" => $categories[$i]["sr_id"]),
                'urlAlias' => $category["urlAlias"], // .'-'.$categories[$i]["innerId"]
            ];

            $urlAlias = querySRApi("/urlAliases", $urlAlias, "POST", "responseBody", false);
            if (isset($urlAlias['urlAlias'])) {
                if (!isset($urlAlias['urlAlias'])) {
                    echo 'urlAlias error<br>';
                    echo $urlAlias['id'] . ':' . $urlAlias['urlAlias'];
                    print_r($urlAlias);
                    echo 'end urlAlias error<br>';
                }
                $categories[$i]["url"] = $urlAlias['urlAlias'];
            } else {
                echo '<br>urlAliasError:';
                print_r($categories[$i]);
                echo 'Dump end<br>';
            }
            // TODO ha mar letezik, akkor torolni kell es ujra rakjuk
            //  print_r($cat['url']);
        } else {

            $urlAliases = querySRApi("/urlAliases?categoryId = " . $categories[$i]["sr_id"], [], "GET", "responseBody");

            foreach ($urlAliases['items'] as $item) {
                $urlAlias = querySRApi(str_replace(SR_APIURL, '', $item['href']), [], "GET", "responseBody");
                $cat['url'] = $urlAlias['urlAlias'];
            }
        }
        $i++;
    }
    return $categories;
}

function skuAvailable($sku) {
    $result = querySRApi(API_ENDPOINT_PRODUCT . '?sku=' . $sku, [], 'GET');
    if (isset($result['items']) && count($result['items']) > 0)
        return true;
    return false;
}

function updateProductParent($sku, $productId, $productParentId) {

    $sr_item = Array(
        'sku' => $sku,
        'parentProduct' => Array
            (
            'id' => $productParentId
        ),
    );

    $sr_product = querySRApi("/products/" . $productId, $sr_item, 'PUT', "responseBody");
    if (!isset($sr_product["id"])) {
        echo 'updateProductParent error<br>';
        print_r($sr_product);
    }
}

function insertSRProduct($product) {


    if (isset($product["category_id"])) {
        $product["categoriess"][] = $product["category_id"];
    }
    $category_id = isset($product["category_id"]) ? $product["category_id"] : '';
    echo $product["sku"] . " > category id " . $category_id . "<br>\n";

    $sr_item = Array(
        'sku' => $product["sku"],
        'modelNumber' => $product["item_number"],
        'price' => $product["price"],
        'stock1' => $product["stock1"],
        'quantity' => $product["quantity"],
        'status' => $product['status'],
        'orderable' => 1,
        'taxClass' => Array
            (
            'id' => TAXCLASS
        ),
    );
    if (isset($product["weight"])) {
        $sr_item['weight'] = $product["weight"];
    }
    if (isset($product["weightUnit"])) {
        $sr_item['weightUnit'] = [
            'id' => $product["weightUnit"]
        ];
    }
    if (isset($product["mainPicture"])) {
        $sr_item['mainPicture'] = $product["mainPicture"];
    }

    if (isset($product["class_id"])) {
        $sr_item['productClass']["id"] = $product["class_id"];
    }
    if (isset($product["manufacturer_id"])) {
        $sr_item['manufacturer']["id"] = $product["manufacturer_id"];
    }

    $sr_product = querySRApi("/products/", $sr_item, 'POST', "responseBody");

    if (isset($sr_product["id"])) {
        // Kategoria osszekapcsolas
        $relation = Array
            (
            'product' => Array
                (
                'id' => $sr_product['id']
            ),
            'category' => Array
                (
                'id' => $category_id
            )
        );
        $response = querySRApi("/productCategoryRelations/", $relation, 'POST', "responseBody");
        // Multiple kategoria osszekapcsolas
        if (isset($product['categories'])) {
            foreach ($product['categories'] as $category_id) {
                $relation = Array
                    (
                    'product' => Array
                        (
                        'id' => $sr_product['id']
                    ),
                    'category' => Array
                        (
                        'id' => $category_id
                    )
                );
                $response = querySRApi("/productCategoryRelations/", $relation, 'POST', "responseBody");
            }
        }
        // Leiras, nev html cuccok
        if (isset($product['proudctDescription'])) {
            $desc = Array
                (
                'name' => $product['proudctDescription']["name"],
                'description' => $product['proudctDescription']["description"],
                'shortDescription' => $product['proudctDescription']["shortDescription"],
                'metaKeywords' => $product['proudctDescription']["metaKeywords"],
                'metaDescription' => $product['proudctDescription']["metaDescription"],
                'product' => Array
                    (
                    'id' => $sr_product['id']
                ),
                'language' => Array
                    (
                    'id' => LANG_HU
                )
            );
            $response = querySRApi("/productDescriptions/", $desc, 'POST', "responseBody");
        }
        // Akcios arak
        if (isset($product['special']) && is_array($product['special'])) {
            foreach ($product['special'] as $special) {
                $special['product']['id'] = $sr_product['id'];
                $special['priority'] = 1;
                $response = querySRApi(API_ENDPOINT_PRODUCT_SPECIAL, $special, 'POST', "responseBody");
            }
        }

        if (isset($product['options']) && is_array($product['options'])) {
            foreach ($product['options'] as $option_name => $values) {
                $prodOption = [
                    'product' => [
                        'id' => $sr_product['id']
                    ]
                ];
                $response = querySRApi(API_ENDPOINT_PRODUCT_OPTION, $prodOption, 'POST', "responseBody");
                if (isset($response['id'])) {
                    $optionDesc = [
                        'name' => $option_name,
                        'productOption' => [
                            'id' => $response['id'],
                        ],
                        'language' => [
                            'id' => LANG_HU
                        ]
                    ];

                    $responsOD = querySRApi(API_ENDPOINT_PRODUCT_OPTION_DESC, $optionDesc, 'POST', "responseBody");
                    $prodOption['productOptionDescriptions'] = [
                        'id' => $response['id']
                    ];

                    $optionValue = [
                        'productOption' => [
                            'id' => $response['id'],
                        ]
                    ];
                    /*
                      $prodOption['productOptionValues'] = [
                      'id' => $responsOV['id']
                      ]; */

                    foreach ($values as $val) {
                        $responsOV = querySRApi(API_ENDPOINT_PRODUCT_OPTION_VALUE, $optionValue, 'POST', "responseBody");
                        $optionValueDesc = [
                            'name' => $val,
                            'productOptionValue' => [
                                'id' => $responsOV['id'],
                            ],
                            'language' => [
                                'id' => LANG_HU
                            ]
                        ];
                        $responsOVD = querySRApi(API_ENDPOINT_PRODUCT_OPTION_VALUE_DESC, $optionValueDesc, 'POST', "responseBody");
                        //TODO lehet valahogy vissza kel lcsatolni ay id-t vagy nem
                    }
                }
            }
        }

        if (isset($product['customerGroup']) && is_array($product['customerGroup'])) {
            $custGroupPrice = $product['customerGroup'];
            $custGroupPrice['product']['id'] = $sr_product['id'];
            $response = querySRApi(API_ENDPOINT_CUSTOMER_GROUP_PRICE, $custGroupPrice, 'POST', "responseBody");
        }

        if (isset($product['images']) && is_array($product['images'])) {
            foreach ($product['images'] as $imagePath) {
                $imagePost = array(
                    'imagePath' => $imagePath,
                    'product' => array('id' => $sr_product['id'])
                );
                $responseImage = querySRApi("/productImages/", $imagePost, 'POST', "responseBody");
            }
        }

        //insert properties
        if (isset($product["properties"])) {
            foreach ($product["properties"] as $property) {
                $req = array
                    (
                    'product' => array('id' => $sr_product["id"]),
                    'listAttributeValue' => array('id' => $property["val"])
                );
                $response = querySRApi("/productListAttributeValueRelations/", $req, 'POST', "responseBody");
                ;
            }
        }

        // Update Ulr aliases
        if ($product["urlAlias"] != '') {
            $urlAlias = [
                'type' => 'PRODUCT',
                'urlAliasEntity' => array("id" => $sr_product["id"]),
                'urlAlias' => $product["urlAlias"],
            ];
            // .'-'.$sr_product["innerId"]
            $urlAlias = querySRApi("/urlAliases", $urlAlias, "POST", "responseBody", false);
            if (!isset($urlAlias['urlAlias'])) {
                print_r($urlAlias);
            }
            $sr_product['url'] = $urlAlias['urlAlias'];
            // TODO ha mar letezik, akkor torolni kell es ujra rakjuk
            //  print_r($cat['url']);
        } else {
            $urlAliases = querySRApi("/urlAliases?productId = " . $sr_product['id'], [], "GET", "responseBody");

            foreach ($urlAliases['items'] as $item) {
                $urlAlias = querySRApi(str_replace(SR_APIURL, '', $item['href']), [], "GET", "responseBody");
                $sr_product['url'] = $urlAlias['urlAlias'];
            }
        }
        return $sr_product;
    } else
        return "";
}

function manageProductClasses(&$productclasses, $class) {
    if (isset($productclasses[$class])) {
        return $productclasses[$class]["id"];
    }

    $req = array
        (
        'name' => $class
    );
    $result = querySRApi("/productClasses", $req, 'POST', "responseBody");
    if (isset($result["id"])) {
        $productclasses[$class]["id"] = $result["id"];
        return $result["id"];
    }

    return "";
}

/*
  $data['attributeKey'] -  Egyedi kulcs pl color
  $data['attributeName'] - Név: pl szín
  $data['attributeDesc'] - Leírás magyar
  $data['productClass'] - Termék típus shoprenter id
  $data['values'] - A termék típus lista értékek pl $data['values'][] = 'piros'
 */

function addAttributeWithValues($data, $debug) {
    if (!isset($data['attributeDesc'])) {
        $data['attributeDesc'] = '';
    }
    echo "new key " . $data['attributeKey'] . "\n";
    //insert new attribute
    $req = Array
        (
        'type' => 'LIST',
        'name' => $data['attributeKey'],
        'presentation' => 'TEXT',
    );

    //insert new attribute desc
    $result = querySRApi("/listAttributes", $req, 'POST', "responseBody", $debug);
    $data['attributeId'] = $result["id"];
    $req = Array
        (
        'name' => $data['attributeName'],
        'description' => $data['attributeDesc'],
        'attribute' => array('id' => $result["id"]),
        'language' => array('id' => LANG_HU)
    );

    $result2 = querySRApi("/attributeDescriptions", $req, 'POST', "responseBody", $debug);


    //connect this new attribute to the product class
    $req = Array
        (
        'productClass' => array('id' => $data['productClass']),
        'attribute' => array('id' => $result["id"])
    );
    $result3 = querySRApi("/productClassAttributeRelations", $req, 'POST', "responseBody", $debug);

    echo "got " . $result["id"] . " as SK key for this category. Storing...\n";

    foreach ($data['values'] as $key => $name) {
        echo "new value " . $name . "\n";
        $req = Array
            (
            'listAttribute' => array('id' => $data['attributeId'])
        );
        $resultAV = querySRApi("/listAttributeValues", $req, 'POST', "responseBody", $debug);

        if (isset($resultAV["id"])) {
            $data['values_ids'][$key] = $resultAV['id'];
            $req = Array
                (
                'listAttributeValue' => array('id' => $resultAV["id"]),
                'language' => array('id' => LANG_HU),
                'name' => $name
            );
            $resultAVD = querySRApi("/listAttributeValueDescriptions", $req, 'POST', "responseBody", $debug);
        } else {
            echo 'attribute value error:';
            print_r($resultAV);
        }
    }
    return $data;
}
