<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace db;

/**
 * Description of ResourceToDB
 *
 * @author szekus
 */
class ResourceToDB {

    private $db;
    private $resource;
    private $tableName;
    private static $prefix = "aaaSR_";

    public static function create(\resources\Resource $resource) {
        $resourceToDB = new ResourceToDB();
        $apiEndpoint = $resource->getApiEndpoint();
        $tableName = substr($apiEndpoint, 1);
        $resourceToDB->tableName = self::$prefix . $tableName;
        return $resourceToDB;
    }

    private function __construct() {
//        $db = Database::instance();
//        $db->connect();
        $this->db = Database::instance();
//        $this->db->connect();
    }

    function getTableName() {
        return $this->tableName;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

    public function getAllResource() {
        $query = "SELECT * FROM " . $this->getTableName();
        $res = $this->db->query($query);
        $result = array();
        foreach ($res as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function updateToDB($resourceDatas) {
        $queryHead = "UPDATE " . $this->getTableName() . " ";
        foreach ($resourceDatas as $resourceData) {

            $firtsElemt = true;
            $where = "";
            $set = "SET ";
            $query = "";
            foreach ($resourceData->getData() as $key => $value) {

                if ($firtsElemt) {
                    $where = "id = '" . $value . "'";
                    $firtsElemt = false;
                } else {


                    $column = $key . "='" . $value . "', ";
                    $set .= $column;
                }


//                $succes = $this->db->query($query);
            }
            $set = substr($set, 0, -2);
            $set .= " ";
            $query .= $set . $where;
//            sout($query);
            $succes = $this->db->query($query);
            $query = $queryHead . $set . $where;

//            sout($query);
        }
    }

    public function insertToDB($resourceDatas) {
//        $resource = $resourceDatas[0];
        $queryHead = "INSERT INTO " . $this->getTableName();
//        $queryCol = "(";
//
//        foreach ($resource->getDataColumns() as $columnName) {
//            $queryCol .= $columnName . ", ";
//        }
//        $queryCol = substr($queryCol, 0, -2);
//        $queryCol .= ")";

        foreach ($resourceDatas as $resourceData) {
            $query = "";
            $queryVal = " VALUES(";
//            sout($resourceData->getData());
//            die();
            foreach ($resourceData->getData() as $key => $value) {

                if (is_array($value)) {
                    $queryVal .= "'" . $value["href"] . "', ";
                } else {
                    $queryVal .= "'" . $value . "', ";
                }
//                $counter++;
            }
            $queryVal = substr($queryVal, 0, -2);
            $queryVal .= "), ";
            $queryValues = $queryVal;
            $queryValues = substr($queryValues, 0, -2);
//            $query = $queryHead . $queryCol . $queryValues;
            $query = $queryHead . $queryValues;
//            sout($query);
            $succes = $this->db->query($query);
        }
    }

    public function createDBTable($resource) {
        $query = "SELECT 1 FROM " . $this->getTableName() . " LIMIT 1";
        sout($query);
        $res = $this->db->query($query);
        sout($res);
        if ($res == false) {
            $query = "CREATE TABLE " . $this->getTableName() . " ( \n";
            $primaryKeyText = " NOT NULL PRIMARY KEY";
            foreach ($resource->getDataColumns() as $value) {

                if ($value == "id") {
                    $query .= $value . " varchar(100) NOT NULL PRIMARY KEY, \n";
                } else {
                    $query .= $value . " TEXT, \n";
                }
            }
            $query = substr($query, 0, -3);
            $query .= ")";
//        sout($query);
            $this->db->query($query);
        } else {
            $query = "DROP TABLE " . $this->getTableName();
            $this->db->query($query);
            sout("BUG");
            $this->createDBTable($resource);
        }
    }

    public function truncateTable() {
        $query = "TRUNCATE $this->tableName";
        $this->db->query($query);
    }

    function getDb() {
        return $this->db;
    }

    function setDb($db) {
        $this->db = $db;
    }

}
