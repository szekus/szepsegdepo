<?php

namespace db;

use PDO;

class Database extends AbstractDatabase {

    protected $tableName;
    private static $_instance;

    private function __construct() {
        $this->dsn = "mysql:"
                . "dbname=" . $this->getDbname() . ";"
                . "host=" . $this->getHost() . ";"
                . "charset=" . $this->getCharset();
//        $this->user = $this->getUser();
//        $this->password = $this->getPassword();

        try {
            $this->connect = new PDO($this->getDsn(), $this->getUser(), $this->getPassword());
//            echo 'Connected<br>';
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }
    }

    private function __clone() {
        
    }

    public static function instance() {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function connect() {
        return $this->connect;
    }

    public function close() {
        $this->connect = null;
    }

    public function query($query) {
        $result = $this->connect->query($query);
        $resultArray = array();
        if ($result != false) {
            foreach ($result as $value) {
                $resultArray[] = $value;
            }
        }
        return $resultArray;
    }

    public function findOneByQuery($query, $key) {
        $res = $this->connect->query($query);
        if ($res != FALSE) {
            foreach ($res as $value) {
                return($value[$key]);
            }
        }
        return "";
    }

    public function insert($data) {
        $queryHead = "INSERT INTO " . $this->getTableName();
        $query = "";
        $queryVal = " VALUES(";
        $queryCol = "(";


        foreach ($data as $key => $value) {

            $queryCol .= $key . ", ";
            $queryVal .= "'" . $value . "', ";

//            sout($value);
        }
//        sout($queryVal);
        $queryCol = substr($queryCol, 0, -2);
        $queryCol .= ")";
        $queryVal = substr($queryVal, 0, -2);
        $queryVal .= "), ";
        $queryValues = $queryVal;
        $queryValues = substr($queryValues, 0, -2);
        $query = $queryHead . $queryCol . $queryValues;
//        sout($query);
//            $query = $queryHead . $queryValues;
        $succes = $this->query($query);
        return $succes;
    }

    public function update($data, $where) {
        $query = "UPDATE " . $this->tableName . " ";
        $set = "SET ";
        $column = "";
        foreach ($data as $key => $value) {
            $column = $key . "='" . $value . "', ";
        }
        $set .= $column;
        $set = substr($set, 0, -2);
        $set .= " ";
        $query .= $set . $where;
//        sout($query);
        $succes = $this->query($query);
        return $succes;
    }

    public function getAllAsArray() {
        $query = "SELECT * FROM " . $this->getTableName();
        $res = $this->query($query);
        $result = array();
        foreach ($res as $value) {
            $result[] = $value;
        }
        return $result;
    }

    public function truncate() {
        $query = "TRUNCATE " . $this->getTableName();
        $this->query($query);
    }

    function getTableName() {
        return $this->tableName;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

}
