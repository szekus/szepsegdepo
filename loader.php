<?php
$cwd = getcwd();
require_once($cwd.'/classes/CSVLoader.php');
require_once($cwd.'/classes/CSVCleaner.php');
require_once($cwd.'/classes/apicall.php');


spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', '/', $class_name);
    include $class_name . '.php';
});