<?php

namespace resources;

use DOMDocument;
use SimpleXMLElement;

/**
 * Description of Resource
 *
 * @author tamas
 */
class Resource {

//    protected $id;
//    protected $innerId;
    protected $apiEndpoint;
    protected $list;
    protected $data = array();
    protected $dataColumns;
    protected $resourceToDB;
   

    public static function create() {
        $resource = new Resource();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        
    }

    public function __get($name) {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
                'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'], E_USER_NOTICE);
        return null;
    }

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

//    public function __toString() {
//        return $id;
//    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setInnerId($innerId) {
        $this->innerId = $innerId;
    }

    public function getInnerId() {
        return $this->innerId;
    }

    function getApiEndpoint() {
        return $this->apiEndpoint;
    }

    function setApiEndpoint($apiEndpoint) {
        $this->apiEndpoint = $apiEndpoint;
    }

    public function getAsArray() {
//        return (get_object_vars($this));
        throw new Exception("NOT USE AGAIN");
    }

    public function insert($debug = false, $type = "POST") {
        $array = $this->getData();
        $result = querySRApi($this->apiEndpoint, $array, $type, "responseBody", $debug);
        return $result;
    }

    public function createSRId($idNumber) {
        return $idNumber;
    }

    public function update($debug = false, $type = "POST") {
//        $array = $this->getAsArray();
        $array = $this->getData();
        $result = querySRApi($this->apiEndpoint . "/" . $this->id, $array, $type, "responseBody", $debug);
        return $result;
    }

    public function deleteAll() {
        $endpointPath = $this->apiEndpoint . "?page=0&limit=200";

        do {
            $result = querySRApi($endpointPath, [], 'GET');
            $batchRequest['requests'] = [];
            foreach ($result['items'] as $item) {
                // print_r($item);
                $batchRequest['requests'][] = [
                    'method' => 'DELETE',
                    'uri' => $item['href']
                ];
                // user data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
            }
            // TORLES
            if (is_array($batchRequest) && count($batchRequest['requests']) > 0) {
                print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
            }
        } while (!is_null($result['next']));

        echo "DELETED \n\n";
    }

    public function soutAll() {
        $result = querySRApi($this->apiEndpoint, [], "GET");
        print_r($result);
    }

    public function setDataFromAPI() {
        $this->data = array();
        $lastPage = $this->getLastPage();
        for ($i = 0; $i <= $lastPage; $i++) {

            $result = querySRApi($this->apiEndpoint . "?page=" . $i, [], "GET", "responseBody", false);
            foreach ($result["items"] as $item) {
                $id = getId($item["href"]);
                $resultResource = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);

                $this->setAllAttributeByArray($resultResource);

                $this->data[] = $this->data;
            }
        }
    }

    public function getAllFromDB() {
        $resources = array();
        $resourcesArray = $this->resourceToDB->getAllResource();
        foreach ($resourcesArray as $resourcesArrayValue) {
            $resource = $this->create();
            $resource->setAllAttributeByArray($resourcesArrayValue);
            $resources[] = $resource;
        }
        return $resources;
    }

    public function deleteFromDB() {
        $this->resourceToDB->truncateTable();
    }

    public function getAll() {
        $resources = array();
        $lastPage = $this->getLastPage();
        for ($i = 0; $i <= $lastPage; $i++) {

            $result = querySRApi($this->apiEndpoint . "?page=" . $i, [], "GET", "responseBody", false);
            foreach ($result["items"] as $item) {
                $id = getId($item["href"]);
                $resultResource = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
//                sout($resultResource);
//                die();
                $resource = $this->create();
                $resource->setAllAttributeByArray($resultResource);

                $resources[] = $resource;
            }
        }
        return $resources;
    }

    public function getBySRId($SRId) {
        $resultResource = querySRApi($this->getApiEndpoint() . "/" . $SRId, [], "GET", "responseBody", false);
//        sout($resultResource);
//        die();
        $resource = $this->create();
        $resource->setAllAttributeByArray($resultResource);
        return $resource;
    }

    public function getByHref($href) {
        return querySRApi("/" . $href, [], "GET", "responseBody", false);
//        sout($resultResource);
//        die();
//        $resource = $this->create();
//        $resource->setAllAttributeByArray($resultResource);
//        return $resource;
    }

    public function getLastPage() {
        $result = querySRApi($this->apiEndpoint, [], "GET", "responseBody", false);
        $last = $result["last"]["href"];
        $parts = parse_url($last);
        parse_str($parts['query'], $query);
        $lastPage = $query['page'];
//        if ($lastPage == 0) {
//            $lastPage = 1;
//        }
        return $lastPage;
    }

    public function getUrlId($url) {
        $arr = explode("/", $url);
        return $arr[count($arr) - 1];
    }

    function getList() {
        return $this->list;
    }

    public function setAllAttributeByArray($array) {
        foreach ($array as $key => $value) {

            if (in_array($key, $this->getDataColumns())) {
                $this->data[$key] = $value;
            }
        }
//        sout("------------------------");
    }

    protected function setUp() {
        foreach ($this->getDataColumns() as $dataColumn) {
            $this->$dataColumn = "";
        }
    }

    function getData() {
        return $this->data;
    }

    function setData($data) {
        $this->data = $data;
    }

    function getResourceToDB() {
        return $this->resourceToDB;
    }

    function setResourceToDB($resourceToDB) {
        $this->resourceToDB = $resourceToDB;
    }

    function getDataColumns() {
        return $this->dataColumns;
    }

    function setDataColumns($dataColumns) {
        $this->dataColumns = $dataColumns;
    }

    public function insertToDB($data) {
        $this->resourceToDB->insertToDB($data);
    }

    public function updateToDB($data) {
        $this->resourceToDB->updateToDB($data);
    }

    public function createDBTable() {
        $this->resourceToDB->createDBTable($this);
    }

    function generateXML() {

//        /* create a dom document with encoding utf8 */
//        $domtree = new DOMDocument('1.0', 'UTF-8');
//
//        /* create the root element of the xml tree */
//        $xmlRoot = $domtree->createElement("xml");
//        /* append it to the document created */
//        $xmlRoot = $domtree->appendChild($xmlRoot);
//
//        $currentTrack = $domtree->createElement("categories");
//        $currentTrack = $xmlRoot->appendChild($currentTrack);
//
//
////        $array = $this->getAllFromDB();
////        foreach ($array as $value) {
////            foreach ($value->getData() as $k => $v) {
////                if ($k != 0) {
////                    $currentTrack->appendChild($domtree->createElement($k, $v));
////                }
////            }
////        }
////        sout($domtree->saveXML());
        $xml = new SimpleXMLElement('<xml/>');
        $parent = $xml->addChild('products');

        $array = $this->getAllFromDB();
        foreach ($array as $value) {
//            sout($value);
//            die();
            $child = $parent->addChild('product');
            foreach ($value->getData() as $k => $v) {
                if ($k != "0") {
                    $child->addChild($k, $v);
                }
            }
        }


//        Header('Content-type: text/xml');
        ($xml->asXML("products.xml"));
    }

    public function createBatchArray() {
        $batch = Batch::create();
        $batch->setUri(SR_APIURL . $this->apiEndpoint);
        $batch->setMethod("POST");
        $batch->setData($this->getData());
        return $batch->createRequestArray();
    }

}
