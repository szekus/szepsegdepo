<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Order
 *
 * @author tamas
 */
class Order extends Resource implements IResource {


    public static function create() {
        $resource = new Order();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orders";
        $this->dataColumns = IResource::ORDER_ARRAY;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    public function createBGOrders($list) {
        $shipping = array();
        $line_items = array();

        $result = array();

        foreach ($list as $item) {

            $order = array();
            
            $order["innerId"] = $item["innerId"];
            
            $shipping["first_name"] = $item["shippingFirstname"];
            $shipping["last_name"] = $item["shippingLastname"];

            $shipping["first_name"] = "TEST";
            $shipping["last_name"] = "TEST";

            if ($item["shippingZoneName"] == "") {
                $shipping["state"] = "CA";
            } else {
                $shipping["state"] = $item["shippingZoneName"];
            }

//            $shipping["state"] = $item["shippingZoneName"];
            $shipping["address_1"] = $item["shippingAddress1"];
            $shipping["address_2"] = $item["shippingAddress2"];
            $shipping["city"] = $item["shippingCity"];

            $shipping["postcode"] = $item["shippingPostcode"];
            $shipping["country"] = $item["shippingCountryName"];
            $order["shipping"] = $shipping;
//            var_dump($item["orderProducts"]["href"]);;
            $hrefs = $this->getProductsLinkFromOrderProductsLink($item["orderProducts"]["href"]);
            $products = $this->getProductsFromOrderProductsLink($hrefs);
            foreach ($products as $prod) {
                $l_item = array();
                $l_item["quantity"] = $prod["quantity"];
                $bgProduct = new \bgresource\BGProduct();
                $bgProduct->getBySku($prod["parent_sku"]);

                if ($prod["parent_sku"] != $prod["child_sku"]) {
                    $arr = explode("-", $prod["child_sku"]);
                    $increment = $arr[count($arr) - 1] - 1;
                    $variations = $bgProduct->getVariations();
                    if (isset($variations[$increment])) {
                        $l_item["product_id"] = $bgProduct->getId();
                        $l_item["variation_id"] = ($variations[$increment]->getId());
                    } else {
                        echo "NO_PRODUCT_ID => ";
                        echo 'sku: ' . $prod["child_sku"] . "\n";
                    }
                } else {
                    $l_item["variation_id"] = $bgProduct->getId();
                }
                $line_items[] = $l_item;
            }
            $order["line_items"] = $line_items;
            $result[] = $order;
//            break;
        }
        return $result;
    }

    public function getProductsLinkFromOrderProductsLink($link) {
        $hrefs = array();
        $id = getParamValue($link, "orderId");
        $url = "/orderProducts?orderId=" . $id;
        $result = querySRApi($url, [], "GET");
//        var_dump($url);
        foreach ($result["items"] as $item) {
            $hrefs[] = $item["href"];
        }
        return $hrefs;
    }

    function getProductsFromOrderProductsLink($hrefs) {
        $products = array();
        foreach ($hrefs as $href) {
            $id = getId($href);
//            echo "/orderProducts/" . $id;
            $result = querySRApi("/orderProducts/" . $id, [], "GET");
//            $products["sku"] = $result["sku"];
            $productHref = $result["product"]["href"];
            $id = getId($productHref);

            $res = querySRApi("/products/" . $id, [], "GET");
            if (!key_exists("error", $res)) {
                $quantity = $result["stock1"];


                for ($i = 2; $i <= 4; $i++) {
                    $stock = "stock" . $i;
                    $quantity += $result[$stock];
                }

//            var_dump($quantity);

                $parentProductHref = $res["parentProduct"]["href"];

                $parentId = getId($parentProductHref);

                $product = array();
                $product["quantity"] = $quantity;
                $product["child_sku"] = $res["sku"];

                if ($id == $parentId) {
                    $product["parent_sku"] = $res["sku"];
                } else {
                    $parentProduct = querySRApi("/products/" . $parentId, [], "GET");
                    $product["parent_sku"] = $parentProduct["sku"];
                }

//                var_dump($parentId);
                $product["child_sku"] = "SIG19069-3";
                $product["parent_sku"] = "SIG19069";

                $products[] = $product;
            }
        }

        return $products;
    }

}
