<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CategoryDescription
 *
 * @author tamas
 */
class CategoryDescription extends Resource {

    public static function create() {
        $resource = new CategoryDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/categoryDescriptions";
        $this->dataColumns = IResource::CATEGORY_DESCRIPTION_ARRAY;
    }

}