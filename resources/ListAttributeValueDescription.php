<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ListAttributeValueDescription
 *
 * @author szekus
 */
class ListAttributeValueDescription extends Resource {

    public static function create() {
        $resource = new ListAttributeValueDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/listAttributeValueDescriptions";
        $this->dataColumns = IResource::LIST_ATTRIBUTE_VALUE_DESCRIPTION_ARRAY;
    }

}
