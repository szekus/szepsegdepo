<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductProductBadgeRelation
 *
 * @author szekus
 */
class ProductProductBadgeRelation extends Resource {
     public static function create() {
        $resource = new ProductProductBadgeRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productProductBadgeRelations";
        $this->dataColumns = IResource::PRODUCT_PRODUCT_BADGE_RELATION_ARRAY;
    }
}
