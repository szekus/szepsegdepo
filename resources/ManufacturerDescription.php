<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ManufacturerDescription
 *
 * @author szekus
 */
class ManufacturerDescription extends Resource {

    private $description;
    private $customTitle;
    private $metaDescription;
    private $metaKeywords;
    private $manufacturer;
    private $language;

    public static function create() {
        $resource = new ManufacturerDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/manufacturerDescriptions";
        $this->dataColumns = IResource::MANUFACTURER_DESCRIPTION_ARRAY;
    }

}
