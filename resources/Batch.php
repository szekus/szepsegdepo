<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Batch
 *
 * @author szekus
 */
class Batch {

    protected $method = "";
    protected $uri = "";
    protected $data = "";

    public static function create() {
        return new Batch();
    }

    private function __construct() {
        
    }

    function getMethod() {
        return $this->method;
    }

    function getUri() {
        return $this->uri;
    }

    function getData() {
        return $this->data;
    }

    function setMethod($method) {
        $this->method = $method;
    }

    function setUri($uri) {
        $this->uri = $uri;
    }

    function setData($data) {
        $this->data = $data;
    }

    public function createRequestArray() {
        $array = array();
        if ($this->getMethod() != "") {
            $array["method"] = $this->getMethod();
        }
        if ($this->getUri() != "") {
            $array["uri"] = $this->getUri();
        }
        if ($this->getData() != "") {
            $array["data"] = $this->getData();
        }

        return $array;
    }

}
