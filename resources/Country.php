<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Costumer
 *
 * @author tamas
 */
class Country extends Resource {

    public static function create() {
        $resource = new Country();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/countries";
        $this->dataColumns = IResource::COUNTRY_ARRAY;
    }

    

    public function getSRCountryIdByIsoCode2($isoCode2) {
        $query = "SELECT id FROM " . $this->getResourceToDB()->getTableName() . " WHERE isoCode2 = '$isoCode2' ";
        return $this->getResourceToDB()->getDb()->findOneByQuery($query, "id");
    }

}
