<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 *
 * @author tamas
 */
interface IResource {

    const ADDRESS_ARRAY = array("id", "innerId", "company", "firstname", "lastname", "taxNumber", "address1", "address2",
        "postcode", "city", "customer", "country", "zone");
    const ATTRIBUTE_DESCRIPTION_ARRAY = array("id", "name", "description", "prefix", "postfix", "attribute", "language");
    const CATEGORY_ARRAY = array("id", "innerId", "picture", "sortOrder", "status", "productsStatus",
        "parentCategory", "centralCategory", "customerGroups", "categoryCustomerGroupRelations");
    const CATEGORY_DESCRIPTION_ARRAY = array("id", "name", "metaKeywords", "metaDescription", "description",
        "robotsMetaTag", "shortDescription", "category", "language");
    const COUNTRY_ARRAY = array("id", "name", "isoCode2", "isoCode3", "status", "zones");
    const CUSTOMER_ARRAY = array("id", "innerId", "firstname", "lastname", "email", "telephone", "fax", "password", "newsletter",
        "status", "approved", "freeShipping", "defaultAdresses", "customerGroup", "loyaltyPoints", "addresses");
    const LIST_ATTRIBUTE_ARRAY = array("id", "type", "name", "priority", "sortOrder", "required", "presentation",
        "defaultListAttributeValue", "listAttributeWidget", "attributeDescriptions", "listAttributeValues");
    const LIST_ATTRIBUTE_VALUE_ARRAY = array("id", "listAttribute", "listAttributeValueDescriptions");
    const LIST_ATTRIBUTE_VALUE_DESCRIPTION_ARRAY = array("id", "name", "listAttributeValue", "language");
    const MANUFACTURER_ARRAY = array("id", "innerId", "name", "picture", "sortOrder", "robotsMetaTag", "manufacturerDescriptions");
    const MANUFACTURER_DESCRIPTION_ARRAY = array("id", "description", "metaDescription", "metaKeywords", "manufacturer", "language");
    const PRODUCT_ARRAY = array("id", "innerId", "sku", "modelNumber", "orderable", "price", "multiplier",
        "multiplierLock", "loyaltyPoints", "stock1", "stock2", "subtractStock", "mainPicture", "width",
        "height", "length", "weight", "status", "imageAlt", "shipped", "minimalOrderNumber", "maximalOrderNumber",
        "minimalOrderNumberMultiply", "availableDate", "quantity", "sortOrder", "freeShipping", "taxClass",
        "noStockStatus", "inStockStatus", "onlyStock1Status", "onlyStock2Status", "productClass", "parentProduct",
        "volumeUnit", "weightUnit", "manufacturer");
    const PRODUCT_CATEGORY_RELATION_ARRAY = array("id", "product", "category");
    const PRODUCT_CLASS_ARRAY = array("id", "name", "description", "firstVariantSelectType",
        "secondVariantSelectType", "firstVariantParameter", "secondVariantParameter", "productClassAttributeRelations");
    const PRODUCT_CLASS_ATTRIBUTE_ARRAY = array("id", "productClass", "attribute");
    const PRODUCT_DESCRIPTION_ARRAY = array("id", "name", "metaKeywords", "metaDescription", "shortDescription", "description",
        "customContentTitle", "customContent", "parameters", "packagingUnit", "measurementUnit", "videoCode", "product", "language");
    const PRODUCT_IMAGES_ARRAY = array("id", "imagePath", "imageAlt", "product");
    const LIST_ATTRIBUTE_VALUE_RELATION_ARRAY = array("id", "product", "listAttributeValue");
    const PRODUCT_RELATED_PRODUCT_RELATION_ARRAY = array("id", "product", "relatedProduct");
    const PRODUCT_PRODUCT_BADGE_RELATION_ARRAY = array("id", "product", "productBadge", "language");
    const PRODUCT_SPECIAL_ARRAY = array("id", "priority", "price", "dateFrom", "dateTo", "minQuantity", "maxQuantity",
        "product", "customerGroup");
    const PRODUCT_TAG_ARRAY = array("id", "product", "language", "tags");
    const ORDER_STATUS_ARRAY = array("id", "orderStatusDescriptions");
    const ORDER_STATUS_DESCRITION_ARRAY = array("id", "name", "color", "orderStatus", "language");
    const ORDER_ARRAY = array("id", "innerId", "invoiceId", "invoicePrefix", "firstname",
        "lastname", "phone", "fax", "email", "shippingFirstname", "shippingLastname", "shippingCompany", "shippingAddress1",
        "shippingAddress2", "shippingCity", "shippingPostcode", "shippingCountryName", "shippingZoneName",
        "shippingAddressFormat", "shippingMethodName", "shippingMethodTaxRate", "shippingMethodTaxName", "shippingMethodExtension",
        "paymentFirstname", "paymentLastname", "paymentCompany",
        "paymentAddress1", "paymentAddress2", "paymentCity", "paymentPostcode", "paymentCountryName", "paymentZoneName",
        "paymentAddressFormat", "paymentMethodName", "paymentMethodCode", "paymentMethodTaxRate", "paymentMethodTaxName",
        "paymentMethodAfter", "taxNumber", "comment", "total", "value", "couponTaxRate", "ip", "pickPackPontShopCode",
        "customer", "customerGroup", "shippingCountry", "shippingZone", "paymentCountry", "paymentZone", "orderStatus",
        "language", "currency", "orderTotals", "orderProducts");
    const URLALIAS_ARRAY = array("id", "type", "urlAliasEntity", "urlAlias");
    const ZONE_ARRAY = array("id", "name", "code", "status", "country");

}
