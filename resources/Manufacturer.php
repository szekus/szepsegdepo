<?php

namespace resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manufacturer
 *
 * @author tamas
 */
class Manufacturer extends Resource {

    public static function create() {
        $resource = new Manufacturer();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/manufacturers";
        $this->dataColumns = IResource::MANUFACTURER_ARRAY;
    }

}
